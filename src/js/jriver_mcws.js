
const toJson = require('xmljson').to_json;
const _ = require('underscore');

export default class JRiverMCWS {
  constructor(host, port) {
    this.host = host;
    this.port = port;

    this.username = 'readonly';
    this.password = 'readonly';

    this.request_header = new Headers();
    const auth = new Buffer(`${this.username}:${this.password}`).toString('base64');
    this.request_header.set('Authorization', `Basic ${auth}`);

    this.base_url = `http://${host}:${port}/MCWS/v1`;
  }

  async request(url, params) {
    const response = await fetch(url, { headers: this.request_header });
    if (!response.ok) {
      throw new Error('request failed.');
    }

    const response_text = await response.text();

    return new Promise((resolve, reject) => {
      toJson(response_text, (error, data) => {
        resolve(data);
      });
    });
  }

  async request_text(url, params) {
    const response = await fetch(url, { headers: this.request_header });
    if (!response.ok) {
      throw new Error('request failed.');
    }

    return response.text();
  }

  getHostURL() {
    return `http://${this.host}:${this.port}`;
  }

  getBaseURL() {
    return this.base_url;
  }

  getAuthorizedURL(url) {
    return `${url}&Token=${this.token}`;
  }

  getBrowseThumbnailUrl(id, stacked=false) {
    // http://pamela.local:52199/MCWS/v1/Browse/Image?Version=2&Token=YODWOUTA&ID=25058&FallbackColor=52%2C188%2C74&UseStackedImages=0&Format=png
    const THUMBNAIL_URL = `${this.base_url}/Browse/Image?ID=${id}&UseStackedImages=${stacked?1:0}`;

    return this.getAuthorizedURL(THUMBNAIL_URL);
  }

  async authenticate() {
    const API_AUTH = `${this.base_url}/Authenticate`;

    const result = await this.request(API_AUTH);
    this.token = this.parseItems(result.Response.Item).Token;

    return true;
  }

  async getAlive() {
    const API_ALIVE = `${this.base_url}/Alive`;

    const result = await this.request(API_ALIVE);
    return this.parseItems(result.Response.Item);
  }

  async getZones() {
    const API_ZONES = `${this.base_url}/Playback/Zones`;

    const result = await this.request(API_ZONES);
    const f = this.xmlFilter.bind(this, result.Response.Item);

    const zones = this.parseIndexedItem(result.Response.Item, [
      { Key: 'ZoneName%', Name: 'Name' },
      { Key: 'ZoneID%', Name: 'Id' },
      { Key: 'ZoneGUID%', Name: 'Guid' },
      { Key: 'ZoneDLNA%', Name: 'Dlna' }
    ]);

    return {
      NumberZones: f('NumberZones'),
      CurrentZoneId: f('CurrentZoneID'),
      CurrentZoneIndex: f('CurrentZoneIndex'),
      Zones: zones
    };
  }

  async getPlaybackInfo(zoneid=-1) {
    const API_PLAYBACKINF = `${this.base_url}/Playback/Info?Zone=${zoneid}`;

    const result = await this.request(API_PLAYBACKINF);
    return this.parseItems(result.Response.Item);
  }

  async getPlaylist(zoneid=-1) {
    const API_PLAYLIST = `${this.base_url}/Playback/Playlist?Zone=${zoneid}`;

    const result = await this.request(API_PLAYLIST);
    return this.parseMPL(result.MPL);
  }

  async getLibraryList() {
    const API_LIB_LIST = `${this.base_url}/Library/List`;

    const result = await this.request(API_LIB_LIST);
    const libraries = this.parseIndexedItem(result.Response.Item, [
      { Key: 'Library%', Name: 'Description' },
      { Key: 'Library%Name', Name: 'Name' },
      { Key: 'Library%Loaded', Name: 'Loaded' }
    ]);

    return {
      NumberOfLibraries: this.xmlFilter(result.Response.Item, 'NumberOfLibraries'),
      Libraries: libraries
    };
  }

  async getLibraryStats() {
    const API_LIB_STATS = `${this.base_url}/Library/GetStats`;

    const result = await this.request(API_LIB_STATS);
    return this.parseItems(result.Response.Item);
  }

  async browse(root_id=-1) {
    const API_BROWSE_CHILDREN = `${this.base_url}/Browse/Children?ID=${root_id}&Version=2`;

    const result = await this.request(API_BROWSE_CHILDREN);
    return this.parseItemsToArray(result.Response.Item);
  }

  async files(id, fileds=[]) {
    const API_BROWSE_FILES = `${this.base_url}/Browse/Files?ID=${id}`;

    const result = await this.request(API_BROWSE_FILES);
    return this.parseMPL(result.MPL);
  }

  async playItems(pl_id, track_id, zoneid=-1) {
    // Get serialised playlist data from playlist id
    const API_FILES_SERIALIZED = `${this.base_url}/Browse/Files?ID=${pl_id}&Action=Serialize`;
    const serialized_pl = await this.request_text(API_FILES_SERIALIZED);

    // Set current playlist by serialized playlist data
    const encoded_pl = encodeURIComponent(serialized_pl);
    const API_SET_PLAYLIST = `${this.base_url}/Playback/SetPlaylist?Zone=${zoneid}&Playlist=${encoded_pl}`;
    await this.request(API_SET_PLAYLIST);

    // Get track index from serialized playlist data
    const index = _.indexOf(serialized_pl.split(';'), track_id) - 3;
    const API_PLAY_BY_INDEX = `${this.base_url}/Playback/PlayByIndex?Zone=${zoneid}&Index=${index}`;
    await this.request(API_PLAY_BY_INDEX);
  }

  async playPause(zoneid=-1) {
    const API_PLAYBACK_PLAYPAUSE = `${this.base_url}/Playback/PlayPause?Zone=${zoneid}`;
    await this.request(API_PLAYBACK_PLAYPAUSE);
  }

  async previous(zoneid=-1) {
    const API_PLAYBACK_PREVIOUS= `${this.base_url}/Playback/Previous?Zone=${zoneid}`;
    await this.request(API_PLAYBACK_PREVIOUS);
  }

  async next(zoneid=-1) {
    const API_PLAYBACK_NEXT = `${this.base_url}/Playback/Next?Zone=${zoneid}`;
    await this.request(API_PLAYBACK_NEXT);
  }

  parseItems(items) {
    const object = {};
    _.values(items).forEach((item) => {
      object[item.$.Name] = item._;
    });

    return items.$ ? _.extend(object, items.$) : object;
  }

  parseItemsToArray(items) {
    const obj_array = [];

    if (!items) {
      return obj_array;
    }

    if (!Object.prototype.hasOwnProperty.call(items, 0)) {
      items = { 0: items };
    }

    _.values(items).forEach((item) => {
      obj_array.push({ Attr: item.$, Value: item._ });
    });

    return obj_array;
  }

  parseIndexedItem(items, keys) {
    const f = this.xmlFilter.bind(this, items);

    const obj_array = [];
    for (let i = 0; ; i++) {
      const obj = {};
      keys.forEach((val) => {
        const key = val.Key.replace('%', i.toString());

        if (f(key)) {
          obj[val.Name] = f(key);
        }
      });

      if (Object.keys(obj).length > 0) {
        obj_array.push(obj);
      } else {
        break;
      }
    }

    return obj_array;
  }

  parseMPL(mpl) {
    const items = [];

    if (!Object.prototype.hasOwnProperty.call(mpl.Item, 0)) {
      mpl.Item = { 0: mpl.Item };
    }

    const num_item = Object.keys(mpl.Item).length;
    for (let i = 0; i < num_item; i++) {
      items.push(this.parseItems(mpl.Item[i].Field));
    }

    return _.extend({
      Items: items
    }, mpl.$);
  }

  xmlFilter(items, name) {
    const item = _.find(items, item => item.$.Name === name);

    return item && item._ ? item._ : undefined;
  }
}
